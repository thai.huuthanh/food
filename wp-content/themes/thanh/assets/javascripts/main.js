$(document).ready(function() {


	$('.slick_slider_home').slick({
		dots: true,
		infinite: true,
		speed: 1000,
		fade: true,
		cssEase: 'linear'
	});


	$('.search_button_d > article').click(function(event) {
		$(this).removeClass('show');
		$('.search_button_d > section').addClass('show');
		$('.search_input').addClass('show');
	});

	$('.search_button_d > section').click(function(event) {
		$(this).removeClass('show');
		$('.search_button_d > article').addClass('show');
		$('.search_input').removeClass('show');
	});

	$('.search_button_m > article').click(function(event) {
		$(this).removeClass('show');
		$('.search_button_m > section').addClass('show');
		$('.search_input form').addClass('show');
	});

	$('.search_button_m > section').click(function(event) {
		$(this).removeClass('show');
		$('.search_button_m > article').addClass('show');
		$('.search_input form').removeClass('show');
	});

	const arr_menu = $('.show_menu_mobile .collapse').toArray();

	$('.menu_button_m').click(function (e) {
		$('.show_menu_mobile').toggleClass('show');
		for (let i = 0; i < arr_menu.length; i++) {
			$(arr_menu[i]).removeClass('show');
		}
	});






	$('html').on('DOMMouseScroll mousewheel', function (e) {
		if($('html,body').scrollTop() > 200){
			$( ".logo" ).addClass( "hidden_header" );
		}else{
			$( ".logo" ).removeClass( "hidden_header" );
		}
		if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
		    //scroll down
		    $( ".header" ).addClass( "hidden" );
		} else {
		    //scroll up
		    $( ".header" ).removeClass( "hidden" );
		}
	});

});
