

<!DOCTYPE html>
<html lang='<?php echo $lang=get_bloginfo("language"); ?>'>
<head> 


	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo get_bloginfo();?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/public/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/public/slick-1.8.1/slick/slick.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/public/slick-1.8.1/slick/slick-theme.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/stylesheets/style.css">
	<style>
		
	</style>
</head>
<body>
	<?php get_header(); ?>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
