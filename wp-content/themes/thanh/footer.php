<footer>
		<article>
			Social Media Discounts
		</article>
		<section>
			Look out for our new special offers and discounts across our social platforms!
		</section>
		<div class="link_word">
			<ul>
				<li>
					<a href="">
						<section>
							<i class="fab fa-facebook-f"></i>
						</section>
					</a>
				</li>
				<li>
					<a href="">
						<section>
							<i class="fab fa-twitter"></i>
						</section>
					</a>
				</li>
				<li>
					<a href="">
						<section>
							<i class="fab fa-google-plus-g"></i>
						</section>
					</a>
				</li>
				<li>
					<a href="">
						<section>
							<i class="fab fa-youtube"></i>
						</section>
					</a>
				</li>
				<li>
					<a href="">
						<section>
							<i class="fab fa-pinterest"></i>
						</section>
					</a>
				</li>
			</ul>
		</div>
		<div class="border_footer">
		</div>
	</footer>
	<div class="copyright">
		© 2020 FarmLand. All Rights Reserved.
	</div>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/public/jquery-3.3.1.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/public/popper.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/public/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/public/fontawesome-free-5.13.0-web/js/all.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/public/slick-1.8.1/slick/slick.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/javascripts/main.js"></script>
</body>
</html>