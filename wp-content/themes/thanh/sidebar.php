<div class="slider_home">
		<div class="slick_slider_home">
			<div class="once_slide">
				<section>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image28.jpg" class="img-fluid" alt="">
				</section>
				<article>
					<h1>
						Summer Discounts!
					</h1>
					<section>
						This summer only we are offering ALL our Berries, Fruits & Vegetables 20% off!
					</section>
					<div class="button_link">
						<a href="">
							<article>
								order now
							</article>
						</a>
					</div>
				</article>
			</div>
			<div class="once_slide">
				<section>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image40.jpg" class="img-fluid" alt="">
				</section>
				<article>
					<h1>
						Seeds come cheaper this Fall…
					</h1>
					<div class="button_link">
						<a href="">
							<article>
								order now
							</article>
						</a>
					</div>
				</article>
			</div>
			<div class="once_slide">
				<section>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image41.jpg" class="img-fluid" alt="">
				</section>
				<article>
					<section>
						Call us now in order to get all of the info on which produce is now on sale and the quotes on our bulk piricing!
					</section>
					<h1>
						1.555.123.2323
					</h1>
					<div class="button_link">
						<a href="">
							<article>
								order now
							</article>
						</a>
					</div>
				</article>
			</div>
		</div>
	</div>
	<div class="welcome">
		<h1>
			WelCome!
		</h1>
		<h3>
			Growing Organic Food Since 1922
		</h3>
		<article>
			While the most recent spike of public interest happened just a couple of decades earlier, at the end of the 90’s, we are not one of those “new” farms.
		</article>
		<section>
			We never were someone who just hopped on the bandwagon of organic food popularity to make some quick profit. In fact, we were in this business since the ’20s, the times when George W. Hershey acquired this farm for the first time and converted it into the biggest fresh & organic produce hub in the local area!
		</section>
		<div class="btn_link">
			<a href="">
				<article>
					Read more
				</article>
			</a>
		</div>
	</div>
	<div class="section_home_1">
		<div class="row plr-md-0 mlr-md-0">
			<div class="col-12 col-md-12 col-lg-6 plr-md-0 image_section_home_1">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image30.jpg" class="img-fluid" alt="">
			</div>
			<div class="col-12 col-md-12 col-lg-6">
				<div class="row">
					<div class="col-12 col-md-6 post_section_home_1">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/blurb1.webp" class="img-fluid" alt="">
						</div>
						<article>
							range of produce
						</article>
						<section>
							While we are a relatively small organic farm, we have a wide range of products that we grow and sell all year long!
						</section>
					</div>
					<div class="col-12 col-md-6 post_section_home_1">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/blurb2.webp" class="img-fluid" alt="">
						</div>
						<article>
							affordable prices
						</article>
						<section>
							It’s thanks to our pricing policy that our organic farm stands out from all the rest of the farms. We make organic food affordable!
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section_home_2">
	<div class="container">
	<div class='row'>
	<div class="col-lg-10">
		<article>
			We’re here to debunk a common misconception about the organic food – <span>especially about it being too expensive</span>. We offer a great variety of organic produce at extremely affordable prices!
		</article>
		</div>
		
	<div class="col-lg-2">
		<section>
			<a href="">
				<article>
					Order Now
				</article>
			</a>
		</section>
	</div>
	</div>
	</div>
	</div>
	<div class="section_home_3">
		<article>
			<section>
				choose any of our organic
			</section>
			<span>
				products!
			</span>
		</article>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image35.jpg" class="img-fluid" alt="">
					</section>
					<article>
						grains
					</article>
				</div>
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image32.jpg" class="img-fluid" alt="">
					</section>
					<article>
						vegetables
					</article>
				</div>
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image48.jpg" class="img-fluid" alt="">
					</section>
					<article>
						fruits
					</article>
				</div>
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image31.jpg" class="img-fluid" alt="">
					</section>
					<article>
						seeds
					</article>
				</div>
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image48.jpg" class="img-fluid" alt="">
					</section>
					<article>
						berries
					</article>
				</div>
				<div class="col-12 col-md-6 col-lg-4 post_section_home_3">
					<section>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image49.jpg" class="img-fluid" alt="">
					</section>
					<article>
						dairy
					</article>
				</div>
			</div>
		</div>
	</div>
	<div class="section_home_4">
		<div class="row plr-md-0 mlr-md-0">
			<div class="col-12 col-md-12 col-lg-6">
				<div class="row">
					<div class="col-12 col-md-6 post_section_home_4">
						<div>
							<i class="fas fa-quote-right"></i>
						</div>
						<article>
							THERESA MAY
						</article>
						<label>
							- June 14, 2016
						</label>
						<section>
							“As an owner of a very small organic farm back in the woods of the Upstate New York, I find myself in a constant need for new seeds to plant to replant. Thanks to this farm I am always able to order them & get delivered to me the next day!”
						</section>
					</div>
					<div class="col-12 col-md-6 post_section_home_4">
						<div>
							<i class="fas fa-quote-right"></i>
						</div>
						<article>
							NIGEL LARAGE
						</article>
						<label>
							- June 14, 2016
						</label>
						<section>
							“I’ve always been a vegetarian and an organic-food movement follower. This means that sometimes it is quite hard for me to purchase something that I want. Thankfully, with online stores like this one, I am always able to place an order, anytime!”
						</section>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-6 plr-md-0 image_section_home_1">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image37.jpg" style="height:auto" class="img-fluid" alt="">
			</div>
		</div>
	</div>
	