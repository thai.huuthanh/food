<header class="header">
		<div class="logo">
		<div class="link_header">
				<ul>
					<li>
						<a href="">
							<section>
								<i class="fab fa-facebook-f"></i>
							</section>
						</a>
					</li>
					<li>
						<a href="">
							<section>
								<i class="fab fa-twitter"></i>
							</section>
						</a>
					</li>
					<li>
						<a href="">
							<section>
								<i class="fab fa-google-plus-g"></i>
							</section>
						</a>
					</li>
					<li>
						<a href="">
							<section>
								<i class="fab fa-youtube"></i>
							</section>
						</a>
					</li>
					<li>
						<a href="">
							<section>
								<i class="fab fa-pinterest"></i>
							</section>
						</a>
					</li>
				</ul>
				<div class="button_header">
					<a href="">
						<section>Book An Appointment</section>
					</a>
				</div>
			</div>
			<div class="is_logo">
			<a href="">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.webp" class="img-fluid" alt="">
			</a>
		</div>
		</div>
		<div class="menu">
			<div class="navbar_menu">
				<section class="search_input">
					<div class="search_input_menu">
						<div class="menu_button_m">
							<i class="fas fa-bars"></i>
						</div>
						<form>
							<label>
								<input type="text" placeholder="Enter keyword">
							</label>
							<section class="btn_search_enter">
								<button type="submit">
									<i class="fas fa-search"></i>
								</button>
							</section>
						</form>
					</div>
					<div class="search_button_m">
						<article class="show">
							<i class="fas fa-search"></i>
						</article>
						<section>
							<i class="fas fa-times"></i>
						</section>
					</div>
				</section>
				<ul class="show_menu_mobile">
					<li class="active">
						<div class="nav_link">
							<a href="">
								<article>
									Home
								</article>
							</a>
							<div class="icon_nav_m collapsed" id="heading1" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
								<i class="fas fa-chevron-down"></i>
							</div>
						</div>
						<div class="nav_menu_1 collapse show" id="collapse1" aria-labelledby="heading1" style="transition: all 0.5s ease;">
							<ul>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
										<div class="icon_nav_m collapsed" id="heading12" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
											<i class="fas fa-chevron-down"></i>
										</div>
									</div>
									<div class="icon_link_nav">
										<section class="icon_d">
											<i class="fas fa-chevron-right"></i>
										</section>
									</div>
									<div class="nav_menu_2 collapse show" id="collapse12" aria-labelledby="heading12" style="transition: all 0.5s ease;">
										<ul>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
										<div class="icon_nav_m collapsed" id="heading13" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
											<i class="fas fa-chevron-down"></i>
										</div>
									</div>
									<div class="icon_link_nav">
										<section class="icon_d">
											<i class="fas fa-chevron-right"></i>
										</section>
									</div>
									<div class="nav_menu_2 collapse show" id="collapse13" aria-labelledby="heading13" style="transition: all 0.5s ease;">
										<ul>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="nav_link">
							<a href="">
								<article>
									Farmers
								</article>
							</a>
							<div class="icon_nav_m collapsed" id="heading2" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
								<i class="fas fa-chevron-down"></i>
							</div>
						</div>
						<div class="nav_menu_1 collapse show" id="collapse2" aria-labelledby="heading2" style="transition: all 0.5s ease;">
							<ul>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="nav_link">
							<a href="">
								<article>
									blog
								</article>
							</a>
							<div class="icon_nav_m collapsed" id="heading3" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
								<i class="fas fa-chevron-down"></i>
							</div>
						</div>
						<div class="nav_menu_1 collapse show" id="collapse3" aria-labelledby="heading3" style="transition: all 0.5s ease;">
							<ul>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
										<div class="icon_nav_m collapsed" id="heading31" data-toggle="collapse" data-target="#collapse31" aria-expanded="false" aria-controls="collapse31">
											<i class="fas fa-chevron-down"></i>
										</div>
									</div>
									<div class="icon_link_nav">
										<section class="icon_d">
											<i class="fas fa-chevron-right"></i>
										</section>
									</div>
									<div class="nav_menu_2 collapse show" id="collapse31" aria-labelledby="heading31" style="transition: all 0.5s ease;">
										<ul>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
										<div class="icon_nav_m collapsed" id="heading32" data-toggle="collapse" data-target="#collapse32" aria-expanded="false" aria-controls="collapse32">
											<i class="fas fa-chevron-down"></i>
										</div>
									</div>
									<div class="icon_link_nav">
										<section class="icon_d">
											<i class="fas fa-chevron-right"></i>
										</section>
									</div>
									<div class="nav_menu_2 collapse show" id="collapse32" aria-labelledby="heading32" style="transition: all 0.5s ease;">
										<ul>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
										<div class="icon_nav_m collapsed" id="heading33" data-toggle="collapse" data-target="#collapse33" aria-expanded="false" aria-controls="collapse33">
											<i class="fas fa-chevron-down"></i>
										</div>
									</div>
									<div class="icon_link_nav">
										<section class="icon_d">
											<i class="fas fa-chevron-right"></i>
										</section>
									</div>
									<div class="nav_menu_2 collapse show" id="collapse33" aria-labelledby="heading33" style="transition: all 0.5s ease;">
										<ul>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
											<li>
												<div class="nav_link">
													<a href="">
														<article>
															menu 1
														</article>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="nav_link">
							<a href="">
								<article>
									products
								</article>
							</a>
							<div class="icon_nav_m collapsed" id="heading4" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
								<i class="fas fa-chevron-down"></i>
							</div>
						</div>
						<div class="nav_menu_1 collapse show" id="collapse4" aria-labelledby="heading4" style="transition: all 0.5s ease;">
							<ul>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
								<li>
									<div class="nav_link">
										<a href="">
											<article>
												menu 1
											</article>
										</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="nav_link">
							<a href="">
								<article>
									contact
								</article>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<div class="search_button_d">
				<article class="show">
					<i class="fas fa-search"></i>
				</article>
				<section>
					<i class="fas fa-times"></i>
				</section>
			</div>
		</div>
	</header>